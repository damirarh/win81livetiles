﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace _4_SettingsCharm.ViewModels
{
    public class OptionsViewModel : INotifyPropertyChanged
    {
        private bool _option1;
        private bool _option2;

        public bool Option1
        {
            get { return _option1; }
            set
            {
                _option1 = value;
                OnPropertyChanged();
                Save();
            }
        }

        public bool Option2
        {
            get { return _option2; }
            set
            {
                _option2 = value;
                OnPropertyChanged();
                Save();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Save()
        {
            // implement immediate saving and applying of settings
        }
    }
}