﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace _1_PushNotifications
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void OnSendNotification(object sender, RoutedEventArgs e)
        {
            var item = new Notification
            {
                Uri = App.Channel.Uri,
                Caption = String.Format("{0} at {1}", CaptionText.Text, DateTime.Now)
            };
            StatusTextBlock.Text = "Sending...";
            await App.MobileService.GetTable<Notification>().InsertAsync(item);
            StatusTextBlock.Text = "Request sent";
        }
    }
}
