﻿namespace _1_PushNotifications
{
    public class Notification
    {
        public string Id { get; set; } 
        public string Uri { get; set; }
        public string Caption { get; set; }
    }
}