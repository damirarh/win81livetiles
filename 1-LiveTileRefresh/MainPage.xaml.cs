﻿using System;
using System.Linq;
using Windows.UI.Notifications;
using Windows.UI.Xaml;

namespace _1_LiveTileRefresh
{
    public sealed partial class MainPage
    {
        private int _badgeValue = 1;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnRefreshTile(object sender, RoutedEventArgs e)
        {
            // templates documetned at http://msdn.microsoft.com/en-us/library/windows/apps/hh761491.aspx
            var xmlDoc = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150PeekImageAndText04);
            var imageNode = xmlDoc.SelectSingleNode("//image[@id='1']");
            var srcAttribute = imageNode.Attributes.Single(a => a.LocalName.ToString() == "src");
            srcAttribute.NodeValue = "ms-appx:///Assets/Logo.scale-100.png";
            var textNode = xmlDoc.SelectSingleNode("//text[@id='1']");
            textNode.InnerText = String.Format("Refreshed at {0}", DateTime.Now);

            var notification = new TileNotification(xmlDoc);
            TileUpdateManager.CreateTileUpdaterForApplication().Update(notification);
        }

        private void OnUpdateBadge(object sender, RoutedEventArgs e)
        {
            var xmlDoc = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);
            var badgeNode = xmlDoc.SelectSingleNode("//badge");
            var valueAttribute = badgeNode.Attributes.Single(a => a.LocalName.ToString() == "value");
            valueAttribute.NodeValue = _badgeValue.ToString();

            var notification = new BadgeNotification(xmlDoc);
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(notification);
            _badgeValue++;
        }
    }
}
