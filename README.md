#Make Your Applications Visible Session Sample Applications

This a set of sample applications accompanying my session about live tiles. The solution consists of 6 projects:

- **1-LiveTileRefresh** demonstrates manual refreshing of liva tiles
- **1-PushNotifications** demonstrates refreshing of live tiles using push notifications (the application must be associated with Windows Store for this to work; also it depends in Windows AzureMobile Service which is not included)
- **2-SecondaryTile** demonstrates the usage of secondary tiles
- **3-ShareCharm** demonstrates the implementation of a share source
- **3-ShareTarget** demonstrates the implementation of a share target
- **4-SettingsCharm** demonstrates the creation of a settings flyout
